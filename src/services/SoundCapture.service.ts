import Sox from "sox-stream";

export default class SoundCapture {
    private sox: typeof Sox;
    private isCapturing: boolean = false;

    constructor() {
        this.sox = Sox;
        this.isCapturing = false;
    }

    startCapture(): NodeJS.ReadableStream {
        this.isCapturing = true;
        const captureStream = this.sox.createReadStream({
            input: {
                type: "alsa",
                device: "default",
                },
                    ut: {
                    type: "wav",
                },
        });

        // Return the capture stream as a readable stream
        return captureStream;
    }

    stopCapture(): void {
        if (this.isCapturing) {
            this.isCapturing = false;
            this.sox.kill();
            console.log("Stopped capturing sound.");
        }
    }
}