import { Router, Request, Response } from "express";
import SoundCapture from "../services/SoundCapture.service";

const soundCapturerouter = Router();
const capture = new SoundCapture();

soundCapturerouter.get("/", (req: Request, res: Response) => {
  res.setHeader("Content-Type", "audio/mpeg");
  res.setHeader("Transfer-Encoding", "chunked");
  res.setHeader("Connection", "keep-alive");
  res.setHeader("Cache-Control", "no-cache");
  res.setHeader("Access-Control-Allow-Origin", "*");

  console.log("streaming");
  const stream = capture.startCapture();
  stream.pipe(res);
});

export default soundCapturerouter;
