import { Router, Request, Response, NextFunction } from "express";

const indexRouter = Router();

/* GET home page. */
indexRouter.get("/", function (req: Request, res: Response, next: NextFunction) {
  res.render("index", { title: "Express" });
});

export default indexRouter;
