# PolyRad WEB

## Name
PolyRad Radio Live streaming plateform

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Requirements
Node JS (LTS version)

Git

Visual Studio Code (any version)

## Installation
Clone the project locally with Git

In root, run: `npm install`

## Usage
`npm install` starts the server
`npm test` tests the serve

## Roadmap
Two modules should be the building blocks of our server. One is the controller that provides APIs to be accesed by the frontend, we'll call this class HTTPController. And another one is a Stremer class that represents an engine to access the music coming from the studio. 

### HTTPController

### Streamer

### Project structure
```
  .
  ├── src                             
    ├── routes                        
      ├── index.js   
      ├── users.js                    
    ├── services                      
      ├── HttpController.service.ts   
      ├── Streamer.service.ts         
  ├── .gitignore  
  ├── .gitlab-ci.yml  
  ├── index.ts             
  ├── README.md                      
  ├── app.js
  ├── package-lock.json
  ├── package.json
  ├── tsconfig.json
  ```

## Project status
Under Construction 

***


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/rachadchazbek17/polyrad-web/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
